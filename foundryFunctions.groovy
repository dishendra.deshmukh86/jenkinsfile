/*
 * Copyright (C) 2022 Esper.IO Inc. All rights Reserved.
 */
import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder

def getFoundryDataByID(url, method, auth) {
    def http = new URL("${url}").openConnection() as HttpURLConnection
    http.setRequestMethod("${method}")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${auth}")
    http.setRequestProperty("Content-Type", "application/json")

    http.connect()

    def response = [:]

    if (http.responseCode == 200) {
        response = new JsonSlurperClassic().parseText(http.inputStream.getText('UTF-8'))
    } else {
        response = new JsonSlurperClassic().parseText(http.errorStream.getText('UTF-8'))
    }

    println "response: ${response}"
    return response.content
}

// API call - Foundry
def foundryAPICall(url, method, auth, body) {
    def http = new URL("${url}").openConnection() as HttpURLConnection
    http.setRequestMethod("${method}")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${auth}")
    http.setRequestProperty("Content-Type", "application/json")

    http.outputStream.write(body.getBytes("UTF-8"))
    http.connect()

    def response = [:]

    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
    }

    println "response: ${response}"
    return response.content.build_id
}

// Pre-build POST call body for Foundry delta build entry
def foundryPreBuildMetaData(source_build_id, targetBuildFoundryData) {
    def bodyMap = [
        "ab_update": targetBuildFoundryData['ab_update'],
        "allow_list": targetBuildFoundryData['allow_list'],
        "android_major_version": targetBuildFoundryData['android_major_version'],
        "android_minimum_sdk_level": targetBuildFoundryData['android_minimum_sdk_level'],
        "android_minor_version": targetBuildFoundryData['android_minor_version'],
        "android_sdk_level": targetBuildFoundryData['android_sdk_level'],
        "block_list": targetBuildFoundryData['block_list'],
        "build_arch": targetBuildFoundryData['build_arch'],
        "build_flavor": targetBuildFoundryData['build_flavor'],
        "build_job_url": "${BUILD_URL}",
        "build_type": "delta",
        "build_url": null,
        "build_update_meta_data": "",
        "build_update_url": null,
        "created": null,
        "foundation_build_number": targetBuildFoundryData['foundation_build_number'],
        "foundation_major_number": targetBuildFoundryData['foundation_major_number'],
        "foundation_minor_number": targetBuildFoundryData['foundation_minor_number'],
        "foundation_name": targetBuildFoundryData['foundation_name'],
        "foundation_variant_name": targetBuildFoundryData['foundation_variant_name'],
        "build_job_params": targetBuildFoundryData['build_job_params'],
        "prebuilt_app_info": targetBuildFoundryData['prebuilt_app_info'],
        "release_notes": targetBuildFoundryData['release_notes'],
        "source_build_id": source_build_id,
        "status": "IN PROGRESS"
    ]

    return bodyMap
}

// Post build API body for Foundry build entry
def foundryPostBuildMetaData(targetBuildFoundryData) {
    def bodyMap = [:]

    // SUCCESS build
    if (currentBuild.currentResult.equals('SUCCESS')) {
        // Build & OTA URL & hash
        def cf_artifacts_url_prefix = "https://ota.esper.io/jenkins_builds/${JOB_NAME}/${BUILD_NUMBER}/artifacts/"
        def artifact_ota_filename = sh(
            script: "find -maxdepth 1 -name \"foundation*\" -type f,l -path \"*delta*.zip\" | sed 's|./||'",
            returnStdout: true
        ).trim()
        if (artifact_ota_filename != "") {
            bodyMap["build_update_url"] = cf_artifacts_url_prefix + artifact_ota_filename
            def buildOTAHash = sh (script: "md5sum ${artifact_ota_filename} | cut -d ' ' -f 1", returnStdout: true).trim()
            bodyMap["build_ota_hash"] = buildOTAHash
        }

        // Build OTA metadata
        def metadata_json
        if (targetBuildFoundryData["ab_update"]) {
            def jsonStr = sh (
                script: "python vendor/esper/scripts/get_ab_metadata.py ${artifact_ota_filename}",
                returnStdout: true
            ).trim()
            metadata_json_slurp = new JsonSlurperClassic().parseText(jsonStr)
            metadata_json_slurp << [name: "${EEA_PROJECT}-${BASE_BUILD_NUMBER}-${TARGET_BUILD_NUMBER}"]
            metadata_json_slurp << [url: bodyMap["build_update_url"]]
            metadata_json_slurp << [ab_install_type: "STREAMING"]
            metadata_json_slurp << [switch_slot_allowed: true]
            metadata_json = new JsonBuilder(metadata_json_slurp).toString()
        } else {
            def otaMd5 = sh (script: "md5sum ${artifact_ota_filename} | cut -d ' ' -f 1", returnStdout: true).trim()
            def metadataMap = [
                "url": bodyMap["build_update_url"],
                "md5hash": otaMd5
            ]
            metadata_json = new JsonBuilder(metadataMap).toString()
        }
        bodyMap["build_update_meta_data"] = metadata_json
    }

    // Created time-stamp
    bodyMap["created"] = sh(script: "date -u +'%Y-%m-%dT%H:%M:%SZ'", returnStdout: true).trim()

    // Status
    def resultMap = [
        "SUCCESS": "SUCCEEDED",
        "FAILURE": "FAILED",
        "ABORTED": "CANCELLED"
    ]
    bodyMap["status"] = resultMap[currentBuild.currentResult]

    return bodyMap
}

return this
