/*
 * Copyright (C) 2022 Esper.IO Inc. All rights Reserved.
 */
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic
import jenkins.plugins.mqttListener.RespMessage
import groovy.time.*

// Global variables

//slack var info
def slackResponse
def slackMessageJobStart
def slackMessageJobEnd
def SLACK_RES_COLOR_MAP = [
        'SUCCESS': 'good',
        'FAILURE': 'danger',
        'ABORTED': 'warning',
]

//foundry var info
def buildFoundryData
def JenkinsHelper

def uuid = null
String scapiUrl = null
def scapiReply = null
RespMessage mqtt_reply
Map<String, String> otaRequest

//Constants
def RETRY_FNM = 3
def RETRY_ONLINE_DUT = 2
def RETRY_PXE_SERVER_STATE = 12
def RETRY_PXE_CLIENT_STATE = 12
def MQTT_REPLY_SLEEP_SEC = 10
def PXE_REPLY_SLEEP_SEC = 20
def RETRY_ONLINE_DUT_INTERVAL_SEC = 240
def PXE_SETUP_DURATION_SEC = 180
def PXE_INSTALL_DURATION_SEC = 180

// Slack message: start
def slackMessageStart() {
    def slackMessage = ''

    // Header
    slackMessage += "OTA Test Automation: ${EEA_PROJECT} ≫ ${BRANCH} ≫ ${TARGET_DEVICE} >> ${BUILD_NUMBER}\n"
    // Triggered by
    def userName = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserName()
    def userId = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()

    slackMessage += "✦ Triggered By: ${userName} (${userId})\n"

    // Job parameters info
    slackMessage += "✦ Build Number: ${BUILD_NUMBER}\n"

    return slackMessage
}


// Slack message: end
def slackMessageEnd() {
    def slackMessage = ''

    // Post build info
    slackMessage += "Result: *${currentBuild.currentResult}* (<${env.BUILD_URL}|link to job>)\n"

    return slackMessage
}

// Slack notification: update progress
def slackNotifUpdateProgress(slackResponse, baseMessage, progress) {
    def slackMessage = baseMessage
    if (!progress.isEmpty()) {
        slackMessage += "\n*Current Stage:* ${progress} :progressloading:\n"
    }
/*     slackSend(
            channel: slackResponse.channelId,
            color: "#439FE0",
            message: slackMessage,
            timestamp: slackResponse.ts
    ) */
}

pipeline {
    agent any
/*     agent {
        node {
            label 'app-builder'
        }
    }
    options {
        ansiColor('xterm')
    } */

    parameters {
        string(name: 'EEA_PROJECT', defaultValue: "falcon", description: 'Foundation Target for Test')
        string(name: 'TARGET_DEVICE', defaultValue: "x86_64", description: 'Foundation Target Arch for Test')
        string(name: 'BRANCH', defaultValue: "eleven", description: 'Android Branch')
        string(name: 'DEVICE_MODEL', defaultValue: "Any", description: 'Device Model')
        string(name: 'brokerUrl', defaultValue: "tcp://localhost:1883", description: 'Broker Url for EFA Automation')
        string(name: 'SCAPI_ENDPOINT', defaultValue: "ypvet", description: 'Endpoint Name of EFA Automation Admin')
        string(name: 'SCAPI_ENTERPRISE_ID', defaultValue: "0d951dbe-3bf0-4214-b973-c47f10ab8123",
                description: 'Enterprise ID EFA Automation Admin')
        string(name: 'BASE_BUILD_NUMBER', defaultValue: "1139", description: 'Build Number for ISO installation')
        string(name: 'TARGET_BUILD_NUMBER', defaultValue: "1140", description: 'Build Number for OTA update')
    }

    environment {
        //MQTT_CREDS = credentials('efa-mqtt-credentials')
        SCAPI_AUTH_KEY = credentials('scapiAuth')
    }

    stages {
        stage("init") {
            steps {
                script {
                    slackMessageJobStart = slackMessageStart()
//                     slackResponse = slackSend(channel: '#foundry-ci-test-space', color: "#439FE0",
//                             message: "${slackMessageJobStart}")

                    //initialise variables
                    mqtt_reply = new RespMessage()
                    otaRequest = new HashMap<String, String>()
                    JenkinsHelper = load "JenkinsHelper.groovy"
                    otaRequest.put("foundation_build_number", BASE_BUILD_NUMBER)
                    scapiUrl = "https://${SCAPI_ENDPOINT}-api.esper.cloud/api/v0/enterprise/${SCAPI_ENTERPRISE_ID}"

                    //1. CHECK_FNM
                    for (int i = 0; i < RETRY_FNM; i++) {
                        otaRequest.put("cmd", "CHECK_FNM")
                        mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: "abcde", mqtt_reply: mqtt_reply)
                        println "The reply received for CHECK_FNM is " + mqtt_reply.reply
                        if (mqtt_reply.reply == "FNM_ONLINE")
                            break
                        else
                            sleep(MQTT_REPLY_SLEEP_SEC)
                    }

                    //2.PREPARE_FNM
                    if (mqtt_reply.reply == "FNM_ONLINE") {
                        mqtt_reply.reply = null

                        otaRequest.put("cmd", "PREPARE_FNM")
                        mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: "abcde", mqtt_reply: mqtt_reply)
                        println "The reply received for PREPARE_FNM is " + mqtt_reply.reply
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: FNM is Down, OTA Failed"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
//                                 timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    if (mqtt_reply.reply == "FNM_READY") {

                        println("Ota Automation Init Stage is Successful!")
                        slackMessageJobStart += "\n✦ Ota Automation Init Stage is Successful!"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
//                                 timestamp: slackResponse.ts)
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: FNM went Down, OTA Failed"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
//                                 timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("prepare-pxe") {
            steps {
                script {
                    //PERFORM_PXE_RECOVERY - START
/*
                    def deviceJob = "OSBuilds/${EEA_PROJECT}/${BRANCH}/${TARGET_DEVICE}"
                    def baseBuildNumber = BASE_BUILD_NUMBER.toInteger()
                    // Check if target-files exists for base build
                    if (JenkinsHelper.fileExists(deviceJob, baseBuildNumber)) {
                        copyArtifacts filter: 'build_metadata.txt',
                                projectName: deviceJob,
                                selector: specific('$BASE_BUILD_NUMBER'),
                                target: 'base_build_artifacts'
                    }
                    def buildMetaDataMap = readProperties file: 'base_build_artifacts/build_metadata.txt'
                    def buildFoundryProdID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_PROD" }?.value
                    def buildFoundryDevID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_DEV" }?.value
                    def baseBuildFoundryData

                    if (buildFoundryProdID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-prod', variable: 'token')]) {
                            try {
                                baseBuildFoundryData = JenkinsHelper.getFoundryDataByID(
                                        "https://foundry-prod.esper.cloud/v0/api/foundry/builds/${buildFoundryProdID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (prod) GET API call failed!*\n"
//                                 slackSend(channel: '#foundry-ci-test-space', color: "#439FE0", message: "${slackMessageJobStart}")
//                                 slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    } else if (buildFoundryDevID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-dev', variable: 'token')]) {
                            try {
                                baseBuildFoundryData = JenkinsHelper.getFoundryDataByID(
                                        "https://foundry-develop.esper.cloud/v0/api/foundry/builds/${buildFoundryDevID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (develop) GET API call failed!*\n"
//                                 slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    }

                    otaRequest.put("android_major_version", baseBuildFoundryData.find { it.key == "android_major_version" }?.value)
                    otaRequest.put("foundation_major_number", baseBuildFoundryData.find { it.key == "foundation_major_number" }?.value)
                    otaRequest.put("foundation_minor_number", baseBuildFoundryData.find { it.key == "foundation_minor_number" }?.value)

                    otaRequest.put("eea_project", baseBuildFoundryData.find { it.key == "foundation_name" }?.value)
                    otaRequest.put("target_device", baseBuildFoundryData.find { it.key == "build_arch" }?.value)
                    otaRequest.put("device_model", DEVICE_MODEL)

                    otaRequest.put("build_iso_url", baseBuildFoundryData.find { it.key == "build_url" }?.value)
                    otaRequest.put("build_iso_hash", baseBuildFoundryData.find { it.key == "build_hash" }?.value)
 */

                    otaRequest.put("android_major_version", "11")
                    otaRequest.put("foundation_major_number", "3")
                    otaRequest.put("foundation_minor_number", "0")

                    otaRequest.put("eea_project", "falcon")
                    otaRequest.put("target_device", "x86_64")
                    otaRequest.put("device_model", DEVICE_MODEL)

                    otaRequest.put("build_iso_url", "https://ota.esper.io/public/eea/falcon/eleven/foundation-11.3.0.1075-x86_64-baremetal-20221020-STAGING.iso")
                    otaRequest.put("build_iso_hash", "b98e550d88b17cdd76b7b0f725de2776")
                    //a. Find online Device
                    for (int i = 0; i < RETRY_ONLINE_DUT; i++) {
                        try {
                            uuid = JenkinsHelper.findOnlineDutForPxe(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        } catch (Exception e) {
                            println("Exception: ${e}")
                            slackMessageJobStart += "\n*:warning: Warning: SCAPI (findOnlineDutForPxe) GET API call failed!*\n"
//                             slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        }
                        if (uuid == null)
                            sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                        else
                            break;
                    }

                    println " The reply received for findOnlineDutForPxe is  " + uuid
                    if (uuid != null) {
                        otaRequest.put("uuid", uuid)
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: DUT Resource Not Available, Abort The Test"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("perform-pxe") {
            steps {
                script {
                    //1.set up pxe and enable server
                    mqtt_reply.reply = null
                    otaRequest.put("cmd", "PXE-SETUP_PXE_SERVER")
                    mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: "abcde", mqtt_reply: mqtt_reply)
                    println "a. The reply received for PXE-SETUP_PXE_SERVER is  " + mqtt_reply.reply

                    if (mqtt_reply.reply != null && mqtt_reply.reply.contains("PXE-SERVER_SETUP_CMD_TRIGGERED")) {
                        sleep(PXE_SETUP_DURATION_SEC)
                        mqtt_reply.reply = null
                        otaRequest.put("cmd", "PXE-CHECK_PXE_SERVER_STATE")
                        for (int i = 0; i < RETRY_PXE_SERVER_STATE; i++) {
                            mqtt_reply.reply = null
                            mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: "abcde", mqtt_reply: mqtt_reply)
                            println "b. The reply received for PXE-CHECK_PXE_SERVER_STATE is  " + mqtt_reply.reply

                            if (mqtt_reply.reply == null)
                                sleep(PXE_REPLY_SLEEP_SEC)
                            else if ((mqtt_reply.reply.contains("PXE-SERVER_SETUP_DONE"))
                                    || (mqtt_reply.reply.contains("PXE-ISO_DOWNLOAD_FAILED"))
                                    || (mqtt_reply.reply.contains("PXE-ISO_MD5_MISMATCH"))
                                    || (mqtt_reply.reply.contains("PXE-DIR_CLEANUP_FAILED"))
                                    || (mqtt_reply.reply.contains("PXE-SERVER_CONFIG_FAILED"))
                                    || (mqtt_reply.reply.contains("PXE-START_DHCP_FAILED"))
                                    || (mqtt_reply.reply.contains("PXE-SERVER_SETUP_FAILED")))
                                break
                        }

                    }

                    return
                    //2. reset the DUT
                    if (mqtt_reply.reply != null && mqtt_reply.reply.contains("PXE-SERVER_SETUP_DONE")) {
                        try {
                            scapiReply = JenkinsHelper.resetDutAndCheckCmdResp(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        } catch (Exception e) {
                            println("Exception: ${e}")
                            slackMessageJobStart += "\n*:warning: Warning: SCAPI (resetDutAndCheckCmdResp) POST API call failed!*\n"
//                             slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        }
                        println "c. The reply received for resetDutAndCheckCmdResp is  " + scapiReply
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: cmd PXE-SETUP_PXE_SERVER failed, Pxe Installation Failed, Abort The Test"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    //3.disable the pxe server
                    if (scapiReply != null && scapiReply.contains("CMD_PASSED")) {
                        sleep(PXE_INSTALL_DURATION_SEC)

                        for (int i = 0; i < RETRY_PXE_CLIENT_STATE; i++) {
                            mqtt_reply.reply = null
                            otaRequest.put("cmd", "PXE-CHECK_PXE_SERVER_DISABLED_STATE")
                            mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: "abcde", mqtt_reply: mqtt_reply)
                            println "d. The reply received for PXE-DISABLE_PXE_SERVER is  " + mqtt_reply.reply

                            if (mqtt_reply.reply == null)
                                sleep(PXE_REPLY_SLEEP_SEC)
                            else if ( (mqtt_reply.reply.contains("PXE-SERVER_DISABLED"))
                                    || (mqtt_reply.reply.contains("PXE-SERVER_DISABLE_FAILED")) )
                                    break;
                        }
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: resetDutAndCheckCmdResp cmd Failed, Pxe Installation Failed, Abort The Test!*\n"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackErrMsg}")
                    }
                    
                    //4.check the result
                    if (mqtt_reply.reply != null && mqtt_reply.reply.contains("PXE-SERVER_DISABLED")) {

                        println("Wait for $RETRY_ONLINE_DUT_INTERVAL_SEC seconds after PXE Installation")
                        sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                        println("Poll for the PXE Installation result")
                        try {
                            scapiReply = JenkinsHelper.pollAndcheckDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        } catch (Exception e) {
                            println("Exception: ${e}")
                            slackMessageJobStart += "\n*:warning: Warning: SCAPI (checkDutStatus) GET API call failed!*\n"
//                             slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        }
                        println("e. The reply received for pollAndcheckDutStatus is  " + scapiReply)

                        try {
                            scapiReply = JenkinsHelper.validateVersionAndgetDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        } catch (Exception e) {
                            println("Exception: ${e}")
                            slackMessageJobStart += "\n*:warning: Warning: SCAPI (validateVersionAndgetDutStatus) GET API call failed!*\n"
//                             slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        }

                        println "f. The reply received for validateVersionAndgetDutStatus is  " + scapiReply
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: PXE-DISABLE_PXE_SERVER cmd Failed, Pxe Recovery Failed, Abort The Test"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    //5. Pase/interpret the installation result
                    if (scapiReply != null && scapiReply.contains("DUT_ONLINE")) {

                        println("Ota Automation pxe-installation Stage is Successful!")
                        slackMessageJobStart += "\n✦ Ota Automation pxe-installation Stage is Successful!"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
//                                 timestamp: slackResponse.ts)
                    } else if (scapiReply != null && scapiReply.contains("DUT_OFFLINE")) {


                        slackMessageJobStart += "\n*:warning: Warning: Resource Unavailable, Abort PXE/OTA Test"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("prepare-ota") {
            steps {
                script {
/*                     def deviceJob = "OSBuilds/${EEA_PROJECT}/${BRANCH}/${TARGET_DEVICE}"
                    def targetBuildNumber = TARGET_BUILD_NUMBER.toInteger()
                    // Check if target-files exists for base build
                    if (JenkinsHelper.fileExists(deviceJob, targetBuildNumber)) {
                        copyArtifacts filter: 'build_metadata.txt',
                                projectName: deviceJob,
                                selector: specific('$TARGET_BUILD_NUMBER'),
                                target: 'target_build_artifacts'
                    }
                    def buildMetaDataMap = readProperties file: 'target_build_artifacts/build_metadata.txt'
                    def buildFoundryProdID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_PROD" }?.value
                    def buildFoundryDevID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_DEV" }?.value
                    def targetBuildFoundryData
                    if (buildFoundryProdID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-prod', variable: 'token')]) {
                            try {
                                targetBuildFoundryData = JenkinsHelper.getFoundryDataByID(
                                        "https://foundry-prod.esper.cloud/v0/api/foundry/builds/${buildFoundryProdID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (prod) GET API call failed!*\n"
//                                 slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    } else if (buildFoundryDevID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-dev', variable: 'token')]) {
                            try {
                                targetBuildFoundryData = JenkinsHelper.getFoundryDataByID(
                                        "https://foundry-develop.esper.cloud/v0/api/foundry/builds/${buildFoundryDevID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (develop) GET API call failed!*\n"
//                                 slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    }

                    otaRequest.put("android_major_version", targetBuildFoundryData.find { it.key == "android_major_version" }?.value)
                    otaRequest.put("foundation_major_number", targetBuildFoundryData.find { it.key == "foundation_major_number" }?.value)
                    otaRequest.put("foundation_minor_number", targetBuildFoundryData.find { it.key == "foundation_minor_number" }?.value)

                    otaRequest.put("build_update_url", targetBuildFoundryData.find { it.key == "build_update_url" }?.value)
                    otaRequest.put("build_ota_hash", targetBuildFoundryData.find { it.key == "build_ota_hash" }?.value)
                    otaRequest.put("foundation_build_number", TARGET_BUILD_NUMBER) */

                   otaRequest.put("android_major_version", "11")
                   otaRequest.put("foundation_major_number", "3")
                   otaRequest.put("foundation_minor_number", "0")

                   otaRequest.put("build_update_url", "https://ota.esper.io/public/eea/falcon/eleven/foundation-11.3.0.1075-x86_64-baremetal-20221020-STAGING-fullota.zip")
                   otaRequest.put("build_ota_hash", "7784ca9822b7141473721c99c79f78be")
                   otaRequest.put("foundation_build_number", TARGET_BUILD_NUMBER)
                }
            }
        }

        stage("perform-ota") {
            steps {
                script {
                    //a. trigger the ota cmd
                    scapiReply = JenkinsHelper.callPerformOtaAndCheckCmdResp(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    println("The reply received for callPerformOtaAndCheckCmdResp:- " + scapiReply)
                    if (scapiReply == "CMD_FAILED") {
                        slackMessageJobStart += "\n*:warning: Warning: DUT OTA cmd Failed, Abort OTA"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    //b. chek the ota cmd result
                    println("Waiting for $RETRY_ONLINE_DUT_INTERVAL_SEC seconds after PERFORM_OTA CMD")
                    sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                    println("Poll for the OTA Update result")

                    scapiReply = JenkinsHelper.pollAndcheckDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    println "The reply received for pollAndcheckDutStatus is  " + scapiReply


                    scapiReply = JenkinsHelper.validateVersionAndgetDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    println("The reply received for validateVersionAndgetDutStatus is " + scapiReply)
                    if (scapiReply == "DUT_ONLINE") {
                        slackMessageJobStart += "\n✦ OTA Update is Successful"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        println "OTA Successful"
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: OTA Failed"
//                         slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }
    }
}