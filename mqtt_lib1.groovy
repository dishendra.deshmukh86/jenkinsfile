import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import groovy.transform.Field;
import groovy.sql.Sql;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.io.Serializable;


class SubscriberCallback implements MqttCallback,Serializable {
    int m_numReceived = 0
    long m_startTime
    boolean firstMessageReceived = false

    private CountDownLatch m_latch = new CountDownLatch(1)

    void waitFinish() {
        m_latch.await()
    }

    public void connectionLost(Throwable cause) {
        m_latch.countDown()
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {
       println "Received message ${new String(message.payload)} on topic [${topic}]"
       if ((topic == "OTATEST_CONTROL_CHANNEL") && (receivedMsg == "Hi Jenkins!!")) {
           println "Disconnecting on OTATEST_CONTROL_CHANNEL"
           //client.disconnect()
           m_latch.countDown()
	    }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
    }
}


def mqttJenkinsListener() { 
    final String tmpDir = System.getProperty("java.io.tmpdir")
    final MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir)

    final int rnd = (Math.random() * 100) as int
    final MqttClient client = new MqttClient("tcp://localhost:1883", "JenkinsSubscriberClient${rnd}", dataStore)

    client.connect()

   
        client.subscribe("OTATEST_CONTROL_CHANNEL", 1, new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) {
                println "Received message ${new String(message.payload)} on topic [${topic}]"
            }
        })

    client.disconnect()

}

return this
