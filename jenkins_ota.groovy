import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import groovy.transform.Field
import groovy.sql.Sql
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class SubscriberCallback implements MqttCallback {
    void waitFinish() {
    }

    public void connectionLost(Throwable cause) {
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {
	    String receivedMsg = new String(message.payload);
	    println "msg received " + receivedMsg;
	    if ((topic == "OTATEST_CONTROL_CHANNEL") && (receivedMsg == "Hi Jenkins!!")) {
		    println "Inside The Condition";
		    pubClient.disconnect();
		    subClient.disconnect();
	    }
    }
    public void deliveryComplete(IMqttDeliveryToken token) {
    }
}
MqttClient pubClient=null;
MqttClient subClient=null;

def mqttJenkinsListener() { 
    String tmpDir = System.getProperty("java.io.tmpdir")
    MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir)

    int rnd = (Math.random() * 100) as int
    subClient = new MqttClient("tcp://test.mosquitto.org:1883", "JenkinsSubClient${rnd}", dataStore)
    def callback = new SubscriberCallback()

    subClient.callback = callback
    subClient.connect()
    subClient.subscribe("OTATEST_CONTROL_CHANNEL", 0)

    callback.waitFinish()
}

def mqttJenkinsPublisher() { 
    String tmpDir = System.getProperty("java.io.tmpdir")
    MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir)

    int rnd = (Math.random() * 100) as int
    pubClient = new MqttClient("tcp://test.mosquitto.org:1883", "JenkinsPubClient${rnd}", dataStore)

    pubClient.connect()
    pubClient.publish("OTATEST_CONTROL_CHANNEL",'Hello FNM!!'.bytes, 0, false)
}

return this

