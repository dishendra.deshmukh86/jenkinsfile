import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import groovy.transform.Field
import groovy.sql.Sql
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


def buildApp() {
	echo 'building the application...'
}

def testApp() {
	echo 'testing the application...'
}

def deployApp() {
	echo 'deploying the application...'
	echo "deploying version ${params.VERSION}"
}


def mqttLister() {
    try {
        MqttClient client = new MqttClient("tcp://localhost:1883", "micasaSubOraSQL", new MemoryPersistence());

                client.setCallback(new MqttCallback() {
                        @Override
                        public void connectionLost(Throwable cause) {
                        // TODO Auto-generated method stub
                        println "MQTT connection lost, cause: $cause";
                        }

                        @Override
                        public void messageArrived(String topic, MqttMessage message) throws Exception {
                        String bericht = message.toString() ;
                        println "Topic: $topic - Payload Msg: $bericht " ;
                        sqlok = updateDatabase(topic, bericht) ; 
                        println "Database result: $sqlok " ;
                        }

                        @Override
                        public void deliveryComplete(IMqttDeliveryToken token) {
                        // TODO Auto-generated method stub
                        println "MQTT delivery complete" ;
                        }
                });

                client.connect();
                        if (!client.isConnected() ) {
                                println "No MQTT broker connection" ;
                        }
                        client.subscribe("BEGIN_OTATEST");
                } catch (MqttException e) {
                        println "MQTT Exception found:" ;
                        e.printStackTrace();
                }
}

MqttClient client=null;

class SubscriberCallback implements MqttCallback {

    int m_numReceived = 0
    long m_startTime
    boolean firstMessageReceived = false
    
    private CountDownLatch m_latch = new CountDownLatch(1)
    
    void waitFinish() {
        m_latch.await()
    }

    public void connectionLost(Throwable cause) {
        m_latch.countDown()
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {
       
       //println "Received message ${new String(message.payload)} on topic [${topic}]"
        if (!firstMessageReceived) {
            m_startTime = System.currentTimeMillis()
            firstMessageReceived = true
        }
        if ((topic == "OTATEST_CONTROL_CHANNEL")  && (receivedMsg == "Hi Jenkins!!")) {
            long stopTime = System.currentTimeMillis()
            long spentTime = stopTime - m_startTime
            
            println "subscriber disconnected, received ${m_numReceived} messages in ${spentTime} ms"
            client.disconnect()
            m_latch.countDown()
        } else {
            println "-received ${message} on ${topic} with QoS ${message.qos}"
            m_numReceived++
        }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}


def mqttJenkinsListener() { 
    String tmpDir = System.getProperty("java.io.tmpdir")
    MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir)

    int rnd = (Math.random() * 100) as int
    client = new MqttClient("tcp://localhost:1883", "JenkinsSubscriberClient${rnd}", dataStore)
    def callback = new SubscriberCallback()

    client.callback = callback
    client.connect()
    client.subscribe("OTATEST_CONTROL_CHANNEL", 0)

   callback.waitFinish()
}


return this
