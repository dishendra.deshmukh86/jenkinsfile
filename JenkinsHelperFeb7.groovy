import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic

def getFoundryDataByID(url, method, auth) {
    def http = new URL("${url}").openConnection() as HttpURLConnection
    http.setRequestMethod("${method}")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${auth}")
    http.setRequestProperty("Content-Type", "application/json")

    http.connect()

    def response = [:]

    if (http.responseCode == 200) {
        response = new JsonSlurperClassic().parseText(http.inputStream.getText('UTF-8'))
    } else {
        response = new JsonSlurperClassic().parseText(http.errorStream.getText('UTF-8'))
    }

    println "response: ${response}"
    return response.content
}

def fileExists(String jobName, int buildNo) {
    def job = Jenkins.instance.getItemByFullName(jobName)
    if (job != null) {
        def build = Jenkins.getInstance().getItemByFullName(jobName).getBuildByNumber(buildNo)
        if (build == null) {
            return false
        }
        def artifacts = build.getArtifacts()
        boolean tfExists = artifacts.any { it =~ /(?i)\.*target_files.*(?:zip)$/ }
        return tfExists
    }
    return false
}

class cmdResp {
    int responseCode
    String response_id
    public cmdResp(int responseCode, String response_id) {
        this.responseCode = responseCode
        this.response_id = response_id
    }
}

def checkCmdResp(scapiUrl, scapiAuth, respId) {

    def http = new URL("${scapiUrl}/command/${respId}/").openConnection() as HttpURLConnection
    http.setRequestMethod("GET");
    http.setRequestProperty("Content-Type", "application/json");
    http.setRequestProperty("Authorization", "Bearer ${scapiAuth}");

    http.connect()
    def response = [:]

    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
        println "checkCmdResp query ${respId} status " + response.status.state

        if ( (response.status.state.contains("Command Success"))  ||
                (response.status.state.contains("Command Acknowledged")) ) {

            return "CMD_PASSED"
        } else
            return "CMD_FAILED"
    } else {

        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        println "response: ${response}"
        return "CMD_FAILED"
    }

}

def triggerHeartBeatCmd(scapiUrl, scapiAuth, uuid) {
    def http
    def dutJsonInfo
    def dutStrInfo
    http = new URL("${scapiUrl}/command/").openConnection() as HttpURLConnection
    dutJsonInfo = new JsonBuilder(command_type:"DEVICE", devices:[uuid], device_type:"all",
            command:"UPDATE_HEARTBEAT")
    dutStrInfo =  dutJsonInfo.toPrettyString()
    http.setRequestMethod("POST")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${scapiAuth}")
    http.setRequestProperty("Content-Type", "application/json")
    http.outputStream.write(dutStrInfo.getBytes("UTF-8"))
    http.connect()

    def response = [:]

    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
        println "triggerHeartBeat query response.id: ${response.id}"
        return response.id
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        println "response: ${response}"
    }
    return null
}


def pollAndcheckDutStatus(scapiUrl, scapiAuth, otaRequest) {
    def cmdResp = null
    def uuid = otaRequest.get("uuid")

    //spend total of 6min in worst case
    println("Keep polling SCAPI for device availability")
    for(int i=0;i<36;i++) {
        String respId = triggerHeartBeatCmd(scapiUrl, scapiAuth, uuid);
        if (respId == null) {
            sleep(10)
            continue
        }
        sleep(10)
        cmdResp = checkCmdResp(scapiUrl, scapiAuth, respId);

        if (cmdResp == "CMD_PASSED") {
            println(uuid + ": device came online")
            return "DUT_ONLINE"
        }
    }
    return "DUT_OFFLINE"
}


def findOnlineDutForPxe(scapiUrl, scapiAuth, otaRequest) {
    def build_eeaProject = otaRequest.get("eea_project")
    def build_targetDevice = otaRequest.get("target_device")
    def build_deviceModel = otaRequest.get("device_model")
    def uuid=null
    def http = new URL("${scapiUrl}/device/").openConnection() as HttpURLConnection
    http.setRequestMethod("GET")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${scapiAuth}")
    http.setRequestProperty("Content-Type", "application/json")
    http.connect()

    def response = [:]
    if (http.responseCode == 200 || http.responseCode == 201) {

        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))

        for(res_itr in response.results) {
            if (res_itr.status == 1) {
                def device_eeaProject = res_itr.softwareInfo.foundationInfo.projectName
                def device_targetDevice = res_itr.softwareInfo.supportedAbi
                def device_deviceModel = res_itr.hardwareInfo.model
                uuid  = res_itr.id

                if (uuid != null && build_eeaProject.equals(device_eeaProject) && build_targetDevice.equals(device_targetDevice)
                        && build_deviceModel.equals("Any"))
                    break;
                else if (uuid != null && build_eeaProject.equals(device_eeaProject) && build_targetDevice.equals(device_targetDevice)
                        && build_deviceModel.equals(device_deviceModel))
                    break;
            }
        }
        println "${uuid}"
        return uuid
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        println "findOnlineDut response: ${response}"
        return null
    }
    return null
}

def validateVersionAndgetDutStatus(scapiUrl, scapiAuth, otaRequest) {
    def uuid = otaRequest.get("uuid")
    String build_androidMajorVersion = otaRequest.get("android_major_version")
    int build_foundationMajorNumber = otaRequest.get("foundation_major_number").toInteger()
    int build_foundationMinorNumber =  otaRequest.get("foundation_minor_number").toInteger()
    int build_buildNumber = otaRequest.get("foundation_build_number").toInteger()

    def http = new URL("${scapiUrl}/device/${uuid}/").openConnection() as HttpURLConnection
    http.setRequestMethod("GET")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${scapiAuth}")
    http.setRequestProperty("Content-Type", "application/json")
    http.connect()

    def response = [:]

    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))

        String device_androidMajorVersion = response.softwareInfo.androidVersion
        int device_foundationMajorNumber = response.softwareInfo.foundationInfo.majorVersion
        int device_foundationMinorNumber = response.softwareInfo.foundationInfo.minorVersion
        int device_buildNumber = response.softwareInfo.foundationInfo.buildNumber
        println("build_androidMajorVersion " + build_androidMajorVersion +  " build_foundationMajorNumber " + build_foundationMajorNumber +
                " build_foundationMinorNumber " + build_foundationMinorNumber + " build_buildNumber " + build_buildNumber)

        println("device_androidMajorVersion " + device_androidMajorVersion +  " device_foundationMajorNumber " + device_foundationMajorNumber +
                " device_foundationMinorNumber " + device_foundationMinorNumber + " device_buildNumber " + device_buildNumber)

        if ( (device_androidMajorVersion.equals(build_androidMajorVersion)) && (device_foundationMajorNumber.equals(build_foundationMajorNumber)) &&
                (device_foundationMinorNumber.equals(build_foundationMinorNumber)) && (device_buildNumber.equals(build_buildNumber)) ) {
            println "DUT_ONLINE"
            return "DUT_ONLINE"
        } else {
            println "DUT_OFFLINE"
            return "DUT_OFFLINE"
        }
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        println "response: ${response}"
        return "DUT_OFFLINE"
    }
}


def resetDut(String scapiUrl, String scapiAuth, String uuid) {
    def http = new URL("${scapiUrl}/command/").openConnection() as HttpURLConnection
    def dutJsonInfo = new JsonBuilder(command_type:"DEVICE", devices:[uuid], device_type:"all",
            command:"REBOOT")
    def dutStrInfo =  dutJsonInfo.toPrettyString()
    http.setRequestMethod("POST")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer ${scapiAuth}")
    http.setRequestProperty("Content-Type", "application/json")
    http.outputStream.write(dutStrInfo.getBytes("UTF-8"))
    http.connect()

    def response = [:]
    def cmdRespObj = new cmdResp(0, null)

    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
        println "resetDut query response.id: ${response.id}"
        cmdRespObj.responseCode = http.responseCode
        cmdRespObj.response_id = response.id
        return cmdRespObj
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        cmdRespObj.responseCode = http.responseCode
        cmdRespObj.response_id = null
        println "response: ${response}"
        return cmdRespObj
    }
}

def resetDutAndCheckCmdResp(scapiUrl, scapiAuth, otaRequest) {
    def uuid = otaRequest.get("uuid");
    if(uuid == null) {
        prinln("DUT uuid passed is NULL")
        return "CMD_FAILED"
    }
    def cmdRespObj = resetDut(scapiUrl, scapiAuth, uuid)

    if ( (cmdRespObj.responseCode == 200 || cmdRespObj.responseCode == 201) && (cmdRespObj.response_id != null ) ){
        for(int i=0;i<3;i++) {
            sleep(15)
            def status =  checkCmdResp(scapiUrl, scapiAuth, cmdRespObj.response_id)
            if (status == "CMD_PASSED")
                return "CMD_PASSED"
        }
    }
    return "CMD_FAILED"
}

public class Extras {
    String metaData
    String otaType
    public Extras(String metaData, String otaType) {
        this.metaData = metaData;
        this.otaType = otaType;
    }
}

public class ActionParams {
    String componentName
    Extras extras
    String intentAction
    String serviceType
    public ActionParams(String componentName, String intentAction, String serviceType, Extras extras) {
        this.componentName = componentName
        this.extras = extras
        this.intentAction = intentAction
        this.serviceType = serviceType
    }

}

public class Script {
    String action
    ActionParams actionParams
    String launchType
    public Script(String action, String launchType, ActionParams actionParams) {
        this.action = action
        this.actionParams = actionParams
        this.launchType = launchType
    }
}
public class CustomSettingsConfig {
    Script[] scripts
    public CustomSettingsConfig(Script[] scripts) {
        this.scripts = scripts
    }
}

public class CommandArgs {
    CustomSettingsConfig custom_settings_config
    public CommandArgs(CustomSettingsConfig custom_settings_config) {
        this.custom_settings_config = custom_settings_config
    }
}

def performOta(scapiUrl, scapiAuth, uuid, buildOtaUrl, buildOtaHash) {
    def http = new URL("${scapiUrl}/command/").openConnection() as HttpURLConnection
    def meta_data = new JsonBuilder(url: buildOtaUrl, md5hash: buildOtaHash)
    def pExtras = new Extras(meta_data.toString(), "TRADITIONAL")

    def pActionParams = new ActionParams("io.esper.otamanager/io.esper.otamanager.OTAUpdateService", "io.esper.otamanager.INSTALL_OTA", "BACKGROUND", pExtras)
    def pScript = new Script("LAUNCH", "SERVICE", pActionParams)
    Script[] arrayScript = [pScript]
    def pCustomSettingsConfig = new CustomSettingsConfig(arrayScript)
    def pCommandArgs = new CommandArgs(pCustomSettingsConfig)
    def ypvet_json = new JsonBuilder(command_type:"DEVICE", command:"UPDATE_DEVICE_CONFIG", command_args: pCommandArgs, devices:[uuid], device_type:"all")
    def body1 =  ypvet_json.toPrettyString()

    println "body: " + body1

    http.setRequestMethod("POST")
    http.setDoOutput(true)
    http.setRequestProperty("Authorization", "Bearer PaTpvGZaD9oCFFz7ilSCuZDwp3SIeP")
    http.setRequestProperty("Content-Type", "application/json")

    println("triggered the OTA")
    http.outputStream.write(body1.getBytes("UTF-8"))
    http.connect()

    def response = [:]
    def cmdRespObj = new cmdResp(0, null)


    if (http.responseCode == 200 || http.responseCode == 201) {
        response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
        cmdRespObj.responseCode = http.responseCode
        cmdRespObj.response_id = response.id
        println "response: ${response}"
        return cmdRespObj
    } else {
        response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
        println "response: ${response}"
        cmdRespObj.responseCode = http.responseCode
        cmdRespObj.response_id = null
        return cmdRespObj
    }
}


def callPerformOta(scapiUrl, scapiAuth, uuid, buildOtaUrl, buildOtaHash) {
    return performOta(scapiUrl, scapiAuth, uuid, buildOtaUrl, buildOtaHash)
}

def callPerformOtaAndCheckCmdResp(scapiUrl, scapiAuth, otaRequest) {
    def uuid = otaRequest.get("uuid")
    def buildOtaUrl = otaRequest.get("build_update_url")
    def buildOtaHash = otaRequest.get("build_ota_hash")
    def cmdRespObj = callPerformOta(scapiUrl, scapiAuth, uuid, buildOtaUrl, buildOtaHash)

    if ( (cmdRespObj.responseCode == 200 || cmdRespObj.responseCode == 201) && (cmdRespObj.response_id != null ) ) {
        String cmdRes = null
        for(int i=0;i<3;i++) {
            sleep(20)
            cmdRes =  checkCmdResp(scapiUrl, scapiAuth, cmdRespObj.response_id)
            if (cmdRes == "CMD_PASSED")
                return "CMD_PASSED"
        }
    }
    return "CMD_FAILED"
}

return this