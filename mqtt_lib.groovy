import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.*
import org.eclipse.paho.client.mqttv3.persist.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


def buildApp() {
    echo "Building the app.."
}

def testApp() {
    echo "Building the app.."
}


def deployApp() {
    echo "Building the app.."
}


/*class SubscriberCallback implements MqttCallback {

    void waitFinish() {
    }

    public void connectionLost(Throwable cause) {
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {
        println "Received message ${new String(message.payload)} on topic [${topic}]"
      }

    public void deliveryComplete(IMqttDeliveryToken token) {
    }
}
*/

/*
MqttClient client = null;
def createJenkinsMqttClient() {
    String tmpDir = System.getProperty("java.io.tmpdir");
    MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir);
    client = new MqttClient("tcp://test.mosquitto.org:1883", "JenkinsServer", dataStore);
    client.connect()
}


def listenFNM() {
    //def callback = new SubscriberCallback()
    //client.callback = callback
    
    client.subscribe("OTATEST_CONTROL_CHANNEL", 0, new SubscriberCallback())
}

def pingFNM() {
    
    //MqttMessage message = new MqttMessage('Hello FNM!!'.bytes)
    //message.setQos(0)

    client.publish('OTATEST_CONTROL_CHANNEL', 'Hello FNM!!'.bytes, 0, false)
}
*/

def IsFNMAvailable() {

//String tmpDir = System.getProperty("java.io.tmpdir");
//MqttDefaultFilePersistence dataStore1 = new MqttDefaultFilePersistence(tmpDir);
//MqttDefaultFilePersistence dataStore2 = new MqttDefaultFilePersistence(tmpDir);
//MemoryPersistence persistence = new MemoryPersistence();

    try {
        int rnd = (Math.random() * 100) as int
        client1 = new MqttClient("tcp://test.mosquitto.org:1883", "JenkinsPublisher${rnd}", new MemoryPersistence());
    } catch (MqttException e) {
        println "MQTT Exception found:" ;
        e.printStackTrace();
    }
/*client1.connect();

client1.publish('OTATEST_CONTROL_CHANNEL', 'Hello FNM!!'.bytes, 0, false)
client1.disconnect()

client2 = new MqttClient("tcp://test.mosquitto.org:1883", "JenkinsSubscriber", dataStore2);
client2.connect();
def callback = new SubscriberCallback()
client2.callback = callback
client2.subscribe("OTATEST_CONTROL_CHANNEL", 0)
println "calline disconnect for both"
*/

}

return this
