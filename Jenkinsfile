/*
 * Copyright (C) 2022 Esper.IO Inc. All rights Reserved.
 */
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic
import jenkins.plugins.mqttListener.RespMessage
import groovy.time.*

// Global variables

//slack var info
def slackResponse
def slackMessageJobStart

//foundry var info
def buildFoundryData
def jenkinsHelper

def uuid = null
def serialNo = null
String scapiUrl = null
def scapiReply = null
RespMessage mqttReply
Map<String, String> otaRequest
Map<String, String> deviceInfo

//Constants
def CTS_MAX_PERIOD = 2400 // CTS_MAX_PERIOD*3*60 sec = 5 days
def RETRY_FNM = 3
def RETRY_ONLINE_DUT = 2
def RETRY_PXE_SERVER_STATE = 12
def RETRY_PXE_CLIENT_STATE = 12
def MQTT_REPLY_SLEEP_SEC = 10
def PXE_REPLY_SLEEP_SEC = 20
def RETRY_ONLINE_DUT_INTERVAL_SEC = 120
def PXE_SETUP_DURATION_SEC = 120
def PXE_INSTALL_DURATION_SEC = 180
def CTS_STATUS_MON_INTERVAL_SEC = 180
def FLASHING_REPLY_SLEEP_SEC = 20
def FLASHING_REPLY_RETRY = 30

// Slack message: start
def slackMessageStart() {
    def slackMessage = ''

    // Header
    slackMessage += "OTA Test Automation: ${EEA_PROJECT} ≫ ${BRANCH} ≫ ${TARGET_DEVICE} >> ${BUILD_NUMBER}\n"
    // Triggered by
    def userName = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserName()
    def userId = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()

    slackMessage += "✦ Triggered By: ${userName} (${userId})\n"

    // Job parameters info
    slackMessage += "✦ Build Number: ${BUILD_NUMBER}\n"

    return slackMessage
}

pipeline {
    agent any
    options {
        ansiColor('xterm')
    }

    parameters {
        string(name: 'EEA_PROJECT', defaultValue: "sparrow", description: 'Foundation Target for Test')
        string(name: 'TARGET_DEVICE', defaultValue: "arm64", description: 'Foundation Target Arch for Test')
        string(name: 'BRANCH', defaultValue: "eleven", description: 'Android Branch')
        string(name: 'DEVICE_MODEL', defaultValue: "Any", description: 'Device Model')
        string(name: 'brokerUrl', defaultValue: "ssl://mqtt.esper.cloud:443", description: 'Broker Url for EFA Automation')
        string(name: 'SCAPI_ENDPOINT', defaultValue: "ypvet", description: 'Endpoint Name of EFA Automation Admin')
        string(name: 'SCAPI_ENTERPRISE_ID', defaultValue: "0d951dbe-3bf0-4214-b973-c47f10ab8123",
                description: 'Enterprise ID EFA Automation Admin')
        string(name: 'BASE_BUILD_NUMBER', defaultValue: "1219", description: 'Build Number for ISO installation')
        string(name: 'TARGET_BUILD_NUMBER', defaultValue: "1269", description: 'Build Number for OTA update')
        string(name: 'PERFORM_CTS', defaultValue: "false", description: 'Flag to perform CTS')
    }

    environment {
        MQTT_CREDS = credentials('efa-mqtt-credentials')
        SCAPI_AUTH_KEY = credentials('scapiAuth')
    }

    stages {
        stage("init") {
            steps {
                script {
                    slackMessageJobStart = slackMessageStart()
                    //slackResponse = slackSend(channel: '#foundry-ci-test-space', color: "#439FE0",
                    //        message: "${slackMessageJobStart}")

                    //initialise variables
                    mqttReply = new RespMessage()
                    otaRequest = new HashMap<String, String>()
                    jenkinsHelper = load "JenkinsHelper.groovy"
                    otaRequest.put("foundation_build_number", BASE_BUILD_NUMBER)
                    otaRequest.put("target_device", TARGET_DEVICE);
                    scapiUrl = "https://${SCAPI_ENDPOINT}-api.esper.cloud/api/v0/enterprise/${SCAPI_ENTERPRISE_ID}"

                    //1. CHECK_FNM
                    for (int i = 0; i < RETRY_FNM; i++) {
                        otaRequest.put("cmd", "CHECK_FNM")
                        mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                                credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                        println "The reply received for CHECK_FNM is " + mqttReply.reply
                        if (mqttReply.reply == "FNM_ONLINE")
                            break
                        else
                            sleep(MQTT_REPLY_SLEEP_SEC)
                    }

                    //2.PREPARE_FNM
                    if (mqttReply.reply == "FNM_ONLINE") {
                        mqttReply.reply = null

                        otaRequest.put("cmd", "PREPARE_FNM")
                        mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                                credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                        println "The reply received for PREPARE_FNM is " + mqttReply.reply
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: FNM is Down, OTA Failed"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
                        //        timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    if (mqttReply.reply == "FNM_READY") {

                        println("Ota Automation Init Stage is Successful!")
                        slackMessageJobStart += "\n✦ Ota Automation Init Stage is Successful!"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
                        //        timestamp: slackResponse.ts)
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: FNM went Down, Test Failed"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
                        //        timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("prepare-flash") {
            steps {
                script {
                    //PERFORM_PXE_RECOVERY - START
                    /*def deviceJob = "OSBuilds/${EEA_PROJECT}/${BRANCH}/${TARGET_DEVICE}"
                    def baseBuildNumber = BASE_BUILD_NUMBER.toInteger()
                    // Check if target-files exists for base build
                    if (jenkinsHelper.fileExists(deviceJob, baseBuildNumber)) {
                        copyArtifacts filter: 'build_metadata.txt',
                                projectName: deviceJob,
                                selector: specific('$BASE_BUILD_NUMBER'),
                                target: 'base_build_artifacts'
                    }
                    def buildMetaDataMap = readProperties file: 'base_build_artifacts/build_metadata.txt'
                    def buildFoundryProdID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_PROD" }?.value
                    def buildFoundryDevID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_DEV" }?.value
                    def baseBuildFoundryData

                    if (buildFoundryProdID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-prod', variable: 'token')]) {
                            try {
                                baseBuildFoundryData = jenkinsHelper.getFoundryDataByID(
                                        "https://foundry-prod.esper.cloud/v0/api/foundry/builds/${buildFoundryProdID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (prod) GET API call failed!*\n"
                                //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    } else if (buildFoundryDevID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-dev', variable: 'token')]) {
                            try {
                                baseBuildFoundryData = jenkinsHelper.getFoundryDataByID(
                                        "https://foundry-develop.esper.cloud/v0/api/foundry/builds/${buildFoundryDevID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (develop) GET API call failed!*\n"
                                //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    }*/

                    otaRequest.put("android_major_version", "11")
                    otaRequest.put("foundation_major_number", "2")
                    otaRequest.put("foundation_minor_number", "2")

                    otaRequest.put("eea_project", "sparrow")
                    otaRequest.put("target_device", "arm64")
                    otaRequest.put("device_model", DEVICE_MODEL)

                    otaRequest.put("build_img_url", "https://ota.esper.io/public/eea/sparrow/eleven/foundation-11.2.2.1219-arm64-gsi-20221221-STAGING.img")
                    otaRequest.put("build_img_hash", "7019df18919881e3b0b8ab6048d549dc")

                    //a. Find online Device
                    for (int i = 0; i < RETRY_ONLINE_DUT; i++) {
                        try {
                            deviceInfo = jenkinsHelper.findOnlineDutForFlash(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        } catch (Exception e) {
                            println("Exception: ${e}")
                            slackMessageJobStart += "\n*:warning: Warning: SCAPI (findOnlineDutForPxe) GET API call failed!*\n"
                            //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        }
                        if (deviceInfo != null) {
                            uuid = deviceInfo.get("uuid")
                            serialNo = deviceInfo.get("serial_no")
                        }

                        if ( (uuid == null) || (serialNo == null) )
                            sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                        else
                            break;
                    }

                    println " The reply received for findOnlineDutForPxe is  " + uuid
                    if ( (uuid != null) && (serialNo != null) ) {
                        otaRequest.put("uuid", uuid)
                        otaRequest.put("serial_no", serialNo)
                        scapiReply = jenkinsHelper.enableAdbAndCheckCmdResp(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                        println "The reply received for enableAdbAndCheckCmdResp is " + scapiReply
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: DUT Resource Not Available, Abort The Test"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("perform-flash") {
            steps {
                script {
                    //1.Flash the System Image
                    mqttReply.reply = null
                    otaRequest.put("cmd", "PERFORM_SYSTEM_FLASH")
                    mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                            credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                    println "a. The reply received for PERFORM_SYSTEM_FLASH is  " + mqttReply.reply

                    if (mqttReply.reply != null && mqttReply.reply.contains("FLASHING_TRIGGERED")) {
                        for(int i = 0; i < FLASHING_REPLY_RETRY; i++) {
                            sleep(FLASHING_REPLY_SLEEP_SEC)
                            otaRequest.put("cmd", "GET_FLASHING_STATUS")
                            mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                                    credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                            println "b. The reply received for PERFORM_SYSTEM_FLASH is  " + mqttReply.reply
                            println
                        }
                    } else {
                        println "Could not trigger system flash"
                        println "Possible reason image not downloadable or device not available"
                        error("Abort the OTA Test")
                    }
                    //2.Check Foundation Version Post Flash
                    println("Waiting for $RETRY_ONLINE_DUT_INTERVAL_SEC seconds after PERFORM_SYSTEM_FLASH CMD")
                    sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                    println("Poll for the System Flash result")

                    try {
                        scapiReply = jenkinsHelper.pollAndcheckDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    } catch (Exception e) {
                        parameters("Exception: ${e}")
                        slackMessageJobStart += "\n*:warning: Warning: SCAPI (checkDutStatus) GET API call failed!*\n"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                    }

                    println "The reply received for pollAndcheckDutStatus is  " + scapiReply

                    try {
                        scapiReply = jenkinsHelper.validateVersionAndgetDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    } catch (Exception e) {
                        println("Exception: ${e}")
                        slackMessageJobStart += "\n*:warning: Warning: SCAPI (validateVersionAndgetDutStatus) GET API call failed!*\n"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                    }
                    println("The reply received for validateVersionAndgetDutStatus is " + scapiReply)

                    if (scapiReply == "DUT_ONLINE") {
                        slackMessageJobStart += "\n✦ PERFORM_FLASH Update is Successful"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        println "PERFORM_FLASH Successful"
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: PERFORM_FLASH Failed"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }

        stage("trigger-and-minitor-cts") {
            steps {
                script {
                    if (PERFORM_CTS == "false")
                        return
                    mqttReply.reply = null
                    def ctsCmd = "run cts --module CtsGestureTestCases --bugreport-on-failure"
                    otaRequest.put("cmd", ctsCmd)
                    mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                            credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                    println "The reply received for RUN_CTS is " + mqttReply.reply

                    if (mqttReply.reply == "CTS_TRIGGERED") {

                        for (int i = 0; i < CTS_MAX_PERIOD; i++) {

                            sleep(20)
                            ctsCmd = "CHECK_CTS_CMD_STATUS"
                            otaRequest.put("cmd", ctsCmd)
                            mqttJenkinsHelper(brokerUrl: brokerUrl, otaRequest: otaRequest, credentialsId: MQTT_CREDS_USR,
                                    credentialsPassword: MQTT_CREDS_PSW.toCharArray(), mqttReply: mqttReply)
                            println "The reply received for CHECK_CTS_CMD_STATUS is " + mqttReply.reply

                            if (mqttReply.reply != null && mqttReply.reply.contains("End of Results"))
                                break;
                            else
                                println mqttReply.reply
                        }
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: FNM Unable to execute the CTS Cmd, Test Failed"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
                        //        timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    slackMessageJobStart += mqttReply.reply
                    //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart,
                    //        timestamp: slackResponse.ts)
                    error("${slackMessageJobStart}")
                }
            }
        }

        stage("prepare-ota") {
            steps {
                script {
                    def deviceJob = "OSBuilds/${EEA_PROJECT}/${BRANCH}/${TARGET_DEVICE}"
                    def targetBuildNumber = TARGET_BUILD_NUMBER.toInteger()
                    // Check if target-files exists for base build
                    if (jenkinsHelper.fileExists(deviceJob, targetBuildNumber)) {
                        copyArtifacts filter: 'build_metadata.txt',
                                projectName: deviceJob,
                                selector: specific('$TARGET_BUILD_NUMBER'),
                                target: 'target_build_artifacts'
                    }
                    def buildMetaDataMap = readProperties file: 'target_build_artifacts/build_metadata.txt'
                    def buildFoundryProdID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_PROD" }?.value
                    def buildFoundryDevID = buildMetaDataMap.find { it.key == "FOUNDRY_BUILD_ID_DEV" }?.value
                    def targetBuildFoundryData
                    if (buildFoundryProdID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-prod', variable: 'token')]) {
                            try {
                                targetBuildFoundryData = jenkinsHelper.getFoundryDataByID(
                                        "https://foundry-prod.esper.cloud/v0/api/foundry/builds/${buildFoundryProdID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (prod) GET API call failed!*\n"
                                //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    } else if (buildFoundryDevID) {
                        withCredentials([string(credentialsId: 'foundry-auth-token-dev', variable: 'token')]) {
                            try {
                                targetBuildFoundryData = jenkinsHelper.getFoundryDataByID(
                                        "https://foundry-develop.esper.cloud/v0/api/foundry/builds/${buildFoundryDevID}",
                                        'GET',
                                        token
                                )
                            } catch (Exception e) {
                                println("Exception: ${e}")
                                slackMessageJobStart += "\n*:warning: Warning: Foundry (develop) GET API call failed!*\n"
                                //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                            }
                        }
                    }

                    otaRequest.put("android_major_version", targetBuildFoundryData.find { it.key == "android_major_version" }?.value)
                    otaRequest.put("foundation_major_number", targetBuildFoundryData.find { it.key == "foundation_major_number" }?.value)
                    otaRequest.put("foundation_minor_number", targetBuildFoundryData.find { it.key == "foundation_minor_number" }?.value)

                    otaRequest.put("build_update_url", targetBuildFoundryData.find { it.key == "build_update_url" }?.value)
                    otaRequest.put("build_ota_hash", targetBuildFoundryData.find { it.key == "build_ota_hash" }?.value)
                    otaRequest.put("foundation_build_number", TARGET_BUILD_NUMBER)
                }
            }
        }

        stage("perform-ota") {
            steps {
                script {
                    //a. trigger the ota cmd
                    scapiReply = jenkinsHelper.callPerformOtaAndCheckCmdResp(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    println("The reply received for callPerformOtaAndCheckCmdResp:- " + scapiReply)
                    if (scapiReply == "CMD_FAILED") {
                        slackMessageJobStart += "\n*:warning: Warning: DUT OTA cmd Failed, Abort OTA"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }

                    //b. chek the ota cmd result
                    println("Waiting for $RETRY_ONLINE_DUT_INTERVAL_SEC seconds after PERFORM_OTA CMD")
                    sleep(RETRY_ONLINE_DUT_INTERVAL_SEC)
                    println("Poll for the OTA Update result")

                    try {
                        scapiReply = jenkinsHelper.pollAndcheckDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    } catch (Exception e) {
                        parameters("Exception: ${e}")
                        slackMessageJobStart += "\n*:warning: Warning: SCAPI (checkDutStatus) GET API call failed!*\n"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                    }

                    println "The reply received for pollAndcheckDutStatus is  " + scapiReply

                    try {
                        scapiReply = jenkinsHelper.validateVersionAndgetDutStatus(scapiUrl, SCAPI_AUTH_KEY, otaRequest)
                    } catch (Exception e) {
                        println("Exception: ${e}")
                        slackMessageJobStart += "\n*:warning: Warning: SCAPI (validateVersionAndgetDutStatus) GET API call failed!*\n"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                    }
                    println("The reply received for validateVersionAndgetDutStatus is " + scapiReply)

                    if (scapiReply == "DUT_ONLINE") {
                        slackMessageJobStart += "\n✦ OTA Update is Successful"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        println "OTA Successful"
                    } else {

                        slackMessageJobStart += "\n*:warning: Warning: OTA Failed"
                        //slackSend(channel: slackResponse.channelId, color: "#439FE0", message: slackMessageJobStart, timestamp: slackResponse.ts)
                        error("${slackMessageJobStart}")
                    }
                }
            }
        }
    }
}